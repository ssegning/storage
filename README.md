# Storage (Minio)

```bash
helm install ssegning-storage ssegning-storage -n minio
````

```bash
helm upgrade ssegning-storage ./ssegning-storage -n minio
````

```bash
helm delete ssegning-storage -n minio
````

## S3-Bucket Url

- https://github.com/imgproxy/imgproxy/blob/master/docs/serving_files_from_s3.md
- https://progapandist.github.io/imgproxy-form/#
- https://docs.imgproxy.net/#/generating_the_url_advanced?id=example
- https://github.com/imgproxy/imgproxy/tree/master/examples
